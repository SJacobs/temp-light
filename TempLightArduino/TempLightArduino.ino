/*
 Name:		TempLightArduino.ino
 Created:	12/27/2015 3:36:18 PM
 Author:	Simin Jacobs
*/

const int SLIDER_PIN_1 = A2;
const int SLIDER_PIN_2 = A1;
const int LED_PIN = 6;
const int BUZZER_PIN = 3;

const int LIGHT_SENS_PIN = A3;
const int TEMP_SENS_PIN = A4;

const int BUTTON_PIN_1 = 10;
const int BUTTON_PIN_2 = 11;
const int BUTTON_PIN_3 = 12;

const char MESSAGE_START_SYMBOL = '#';
const char MESSAGE_END_SYMBOL = '%';

String command = "";
int prevButtonState;
int buttonState;

int lastSlider1Value = 0;
int lastSlider2Value = 0;
int lastTemp = 0;
int lastLight = 0;

// the setup function runs once when you press reset or power the board
void setup()
{
	pinMode(SLIDER_PIN_1, INPUT);
	pinMode(SLIDER_PIN_2, INPUT);

	pinMode(LED_PIN, OUTPUT);
	pinMode(BUZZER_PIN, OUTPUT);

	pinMode(LIGHT_SENS_PIN, OUTPUT);
	pinMode(TEMP_SENS_PIN, OUTPUT);

	pinMode(BUTTON_PIN_1, INPUT_PULLUP);
	pinMode(BUTTON_PIN_2, INPUT_PULLUP);
	pinMode(BUTTON_PIN_3, INPUT_PULLUP);

	Serial.begin(9600);
	Serial.println("Arduino started.");
}

// the loop function runs over and over again until power down or reset
void loop()
{
	handleButtons();
	handleIncomingMessages();
	sendMessages();
}

void handleButtons() {

	if (digitalRead(BUTTON_PIN_1) == LOW)
	{
		turnBuzzerOff();
		return;
	}

	if (digitalRead(BUTTON_PIN_2) == LOW)
	{
		turnBuzzerOff();
		return;
	}

	if (digitalRead(BUTTON_PIN_3) == LOW)
	{
		turnBuzzerOff();
		return;
	}
}

void handleIncomingMessages()
{
	// Stop if no serial available.
	if (!Serial.available())
	{
		return;
	}

	// Read incoming byte.
	char incomingByte = (char)Serial.read();

	// If the first chararacter of command is not the start symbol, return.
	if (incomingByte != MESSAGE_START_SYMBOL
		&& command.length() == 0)
	{
		return;
	}

	// Append the character.
	command.concat(incomingByte);

	if (incomingByte == MESSAGE_END_SYMBOL)
	{
		parseCommand();
		handleCommand();
	}
}

// Does actions depending on the command.
void handleCommand()
{
	Serial.print("command used: ");
	Serial.println(command);

	if (command == "BUZZER_ON")
	{
		buzzerAction();
	}
	else if (command == "BUZZER_OFF")
	{
		turnBuzzerOff();
	}
	else
	{
		Serial.print("unrecognized command: ");
		Serial.println(command);
	}

	command = "";
}

// Removes first and last character.
void parseCommand()
{
	command.remove(0, 1);
	command.remove(command.length() - 1);
}

void sendMessages()
{
	sendSlider1Value();
	sendSlider2Value();
	sendTemperature();
	sendLight();
}

// max temp
void sendSlider1Value()
{
	int readValue = analogRead(SLIDER_PIN_1);
	int convertedValue = convertSliderToTemperature(readValue);
	Serial.print("#SET_MAX_TEMP:");
	Serial.print(convertedValue);
	Serial.print("%");
}

// min light
void sendSlider2Value()
{
	int readValue = analogRead(SLIDER_PIN_2);
	int convertedValue = convertSliderToLight(readValue);
	Serial.print("#SET_MIN_LIGHT:");
	Serial.print(convertedValue);
	Serial.print("%");
}

void sendTemperature()
{
	int readValue = analogRead(TEMP_SENS_PIN);
	Serial.print("#SET_TEMP:");
	Serial.print(readValue);
	Serial.print("%");
}

void sendLight()
{
	int readValue = analogRead(LIGHT_SENS_PIN);
	Serial.print("#SET_LIGHT:");
	Serial.print(readValue);
	Serial.print("%");
}

void turnBuzzerOff()
{
	noTone(BUZZER_PIN);
}

int convertSliderToTemperature(int value) {
	int convertedValue = map(value, 0, 1000, 100, 0);
	return (convertedValue >= 0) ? convertedValue : 0;
}

int convertSliderToLight(int value) {
	int convertedValue = map(value, 0, 1000, 10, 0);
	return (convertedValue >= 0) ? convertedValue : 0;
}

void buzzerAction()
{
	tone(BUZZER_PIN, 1000);
}
